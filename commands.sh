#!/usr/bin/env bash
function codeIdea() {
  nohup idea.sh $1 &
  >>/dev/null
  clear
  echo "[$(pwd)] -> Abriendo IntelliJ"
}
function reinit() {
  clear
  echo "[./bashrc] -> Recargado"
  source ~/.bashrc
}
GIT_REPO_URL="/home/reno/GitRepo/"
function gitrepo() {
  clear
  echo "[$GIT_REPO_URL]"
  cd "$GIT_REPO_URL"
}
function fp() {
  git add .
  git commit -m "$1"
  git push
}
function config() {
  codeIdea /home/reno/misScripts/
}
